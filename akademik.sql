-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2019 at 04:12 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswas`
--

CREATE TABLE `mahasiswas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswas`
--

INSERT INTO `mahasiswas` (`id`, `nim`, `nama`, `jurusan`, `telepon`, `created`, `modified`) VALUES
(1, '155410177', 'Muhammad Syahrul Anwar', 'Teknik Informatika', '082325557573', '2019-11-02 12:15:41', '2019-11-02 12:15:41'),
(3, '155410179', 'Haris Dwi Kuncahya', 'Sistem Informasi', '08723242425', '2019-11-02 12:16:59', '2019-11-02 12:16:59'),
(4, '155410182', 'Muhammad Qorib', 'Komputerisasi Akutansi', '082322343550', '2019-11-02 12:17:39', '2019-11-02 13:00:08'),
(5, '155410172', 'Wisnu Nugroho', 'Manajemen Informatika', '08347868768', '2019-11-02 12:18:07', '2019-11-02 12:18:07'),
(6, '155410186', 'Agusman Jaya', 'Teknik Informatika', '082343353590', '2019-11-02 12:18:32', '2019-11-02 12:18:32'),
(7, '155410170', 'Abadillah Widodo Hanif H', 'Manajemen Informatika', '085687874903', '2019-11-02 12:19:00', '2019-11-02 12:19:00'),
(8, '155410176', 'Muhammad Zaki Asyari', 'Sistem Informasi', '083535345055', '2019-11-02 13:28:02', '2019-11-02 13:28:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mahasiswas`
--
ALTER TABLE `mahasiswas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mahasiswas`
--
ALTER TABLE `mahasiswas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
