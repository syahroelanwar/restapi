<?php  
class MahasiswasController extends AppController{

    public $components = array('RequestHandler');

    public function index() {
        $mahasiswas = $this->Mahasiswa->find('all');
        $this->set(array(
            'mahasiswas' => $mahasiswas,
            '_serialize' => array('mahasiswas')
        ));
    }

    public function view($id) {
    	$this->layout = false;
        $mahasiswa = $this->Mahasiswa->findById($id);

        if($mahasiswa!=null){
	        $this->set(array(
	            'mahasiswa' => $mahasiswa,
	            '_serialize' => array('mahasiswa')
	        ));
    	}else{
    		$message = 'Error, Couldn\'t found data';
    		$this->set(array(
	            'message' => $message,
	            '_serialize' => array('message')
	        ));
    	}
    }

    public function add() {
    	$this->layout = false;
        //$this->Mahasiswa->create();
        $data = $this->request->input('json_decode',true);
        if(empty($data)){
        	$data = $this->request->data;
        }
        if ($this->Mahasiswa->save($data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

    public function edit($id) {
    	$this->layout = false;
        $this->Mahasiswa->id = $id;

        $data = $this->request->input('json_decode',true);
        if(empty($data)){
        	$data = $this->request->data;
        }
        if ($this->Mahasiswa->save($data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

    public function delete($id) {
    	$this->layout = false;
        if ($this->Mahasiswa->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
}

?>